package pe.com.tpp.ac.quotation.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pe.com.tpp.ac.quotation.model.Quotation;

import java.util.Date;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class QuotationAPITest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getQuotationApiTest() throws Exception{

        Quotation quotationServiceRequest = new Quotation();
        quotationServiceRequest.setUser("jvalencia");

        mvc.perform( MockMvcRequestBuilders
                .post("/v1/quotation/list")
                .content(asJsonString(quotationServiceRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].user").value("admin"));
    }

    @Test
    public void saveQuotationApiTest() throws Exception{

        Quotation quotationServiceRequest = new Quotation();
        quotationServiceRequest.setEnterprise(1);
        quotationServiceRequest.setBusinessUnit(2);
        quotationServiceRequest.setClient(900);
        quotationServiceRequest.setAffair("TEST");
        quotationServiceRequest.setDateOfIssue(new Date());
        quotationServiceRequest.setLandfall(900);
        quotationServiceRequest.setBl(900);
        quotationServiceRequest.setRegion(1);
        quotationServiceRequest.setLocation(1);
        quotationServiceRequest.setCredit(1);
        quotationServiceRequest.setCreditTerm(1);
        quotationServiceRequest.setCampus(1);
        quotationServiceRequest.setCreditInstrument(1);
        quotationServiceRequest.setEndDateOfProforma(new Date());
        quotationServiceRequest.setEndDateOfCont(new Date());
        quotationServiceRequest.setSafe(true);
        quotationServiceRequest.setCustomerType(1);
        quotationServiceRequest.setVerticalMarket(1);
        quotationServiceRequest.setProduct(1);
        quotationServiceRequest.setFinalClient(1);
        quotationServiceRequest.setLoadMonitoring(1);
        quotationServiceRequest.setModality(1);
        quotationServiceRequest.setRegistrationStatus(1);
        quotationServiceRequest.setUser("admin");
        quotationServiceRequest.setProforma("");


        mvc.perform( MockMvcRequestBuilders
                .post("/v1/quotation/add")
                .content(asJsonString(quotationServiceRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateQuotationApiTest() throws Exception{
        Quotation quotationServiceRequest = new Quotation();
        quotationServiceRequest.setId(1);
        quotationServiceRequest.setEnterprise(1);
        quotationServiceRequest.setBusinessUnit(2);
        quotationServiceRequest.setClient(900);
        quotationServiceRequest.setAffair("TEST");
        quotationServiceRequest.setDateOfIssue(new Date());
        quotationServiceRequest.setLandfall(900);
        quotationServiceRequest.setBl(900);
        quotationServiceRequest.setRegion(1);
        quotationServiceRequest.setLocation(1);
        quotationServiceRequest.setCredit(1);
        quotationServiceRequest.setCreditTerm(1);
        quotationServiceRequest.setCampus(1);
        quotationServiceRequest.setCreditInstrument(1);
        quotationServiceRequest.setEndDateOfProforma(new Date());
        quotationServiceRequest.setEndDateOfCont(new Date());
        quotationServiceRequest.setSafe(true);
        quotationServiceRequest.setCustomerType(1);
        quotationServiceRequest.setVerticalMarket(1);
        quotationServiceRequest.setProduct(1);
        quotationServiceRequest.setFinalClient(1);
        quotationServiceRequest.setLoadMonitoring(1);
        quotationServiceRequest.setModality(1);
        quotationServiceRequest.setRegistrationStatus(1);
        quotationServiceRequest.setUser("admin");
        quotationServiceRequest.setProforma("");


        mvc.perform( MockMvcRequestBuilders
                .post("/v1/quotation/update")
                .content(asJsonString(quotationServiceRequest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
