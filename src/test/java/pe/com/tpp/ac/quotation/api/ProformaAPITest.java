package pe.com.tpp.ac.quotation.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import pe.com.tpp.ac.quotation.model.FilterRelatedService;
import pe.com.tpp.ac.quotation.model.RelatedService;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ProformaAPITest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getRelatedServiceGroupApiTest() throws Exception{

        FilterRelatedService filterRelatedServiceTest = new FilterRelatedService();
        filterRelatedServiceTest.setQuotation(1);
        filterRelatedServiceTest.setType(1);
        filterRelatedServiceTest.setOrigin(1);

        mvc.perform( MockMvcRequestBuilders
                .post("/v1/proforma/relatedServiceGroup")
                .content(asJsonString(filterRelatedServiceTest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    public void saveRelatedServiceApiTest() throws  Exception{

        RelatedService relatedServiceTest = new RelatedService();
        //add campos

        mvc.perform( MockMvcRequestBuilders
                .post("/v1/proforma/addRelatedService")
                .content(asJsonString(relatedServiceTest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    public void updateRelatedServiceApiTest() throws  Exception{

        RelatedService relatedServiceTest = new RelatedService();
        //add campos

        mvc.perform( MockMvcRequestBuilders
                .post("/v1/proforma/updateRelatedService")
                .content(asJsonString(relatedServiceTest))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
