package pe.com.tpp.ac.quotation.service;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import pe.com.tpp.ac.quotation.model.FilterQuotation;
import pe.com.tpp.ac.quotation.model.Quotation;

import java.util.List;

@SpringBootTest
@ActiveProfiles("test")
public class QuotationServiceTest {

    @Autowired
    private QuotationService quotationService;

    @Test
    public void listQuotation(){
        FilterQuotation filterQuotationRequest = new FilterQuotation();

        List<Quotation> list = quotationService.list(filterQuotationRequest);
        Assert.assertEquals(list.size(),1);
    }

    @Test
    public void insertQuotation(){
        Quotation quotationRequest = new Quotation();

        Quotation quotationInsertResponse = quotationService.insert(quotationRequest);
        Assert.assertNotEquals(quotationInsertResponse,null);
    }

    @Test
    public void updateQuotation(){
        Quotation quotationRequest = new Quotation();

        Quotation quotationUpdateResponse = quotationService.update(quotationRequest);
        Assert.assertNotEquals(quotationUpdateResponse,null);
    }
}
