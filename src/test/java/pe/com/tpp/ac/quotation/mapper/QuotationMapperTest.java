package pe.com.tpp.ac.quotation.mapper;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import pe.com.tpp.ac.quotation.dao.QuotationMapper;
import pe.com.tpp.ac.quotation.model.FilterQuotation;
import pe.com.tpp.ac.quotation.model.Quotation;

import java.util.List;

@MybatisTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class QuotationMapperTest {

    @Autowired
    private QuotationMapper quotationMapper;

    @Test
    public void findPackageInsertQuotation(){
        Quotation quotationRequest = new Quotation();
        //add
        quotationMapper.insert(quotationRequest);

        Assert.assertNotNull(quotationRequest.getId());
    }

    @Test
    public void findPackageUpdateQuotation(){
        Quotation quotationRequest = new Quotation();
        //add
        quotationMapper.insert(quotationRequest);

        Assert.assertNotNull(quotationRequest.getId());
    }

    @Test
    public void findPackageListQuotation(){
        FilterQuotation filterQuotationRequest = new FilterQuotation();
        List<Quotation> listPackageQuotation = quotationMapper.listQuotation(filterQuotationRequest);

        Assert.assertEquals(listPackageQuotation.size(),1);
    }

}
