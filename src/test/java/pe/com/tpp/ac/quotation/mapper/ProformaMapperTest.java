package pe.com.tpp.ac.quotation.mapper;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import pe.com.tpp.ac.quotation.dao.ProformaMapper;
import pe.com.tpp.ac.quotation.model.*;

import java.util.List;

@MybatisTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ProformaMapperTest {

    @Autowired
    private ProformaMapper proformaMapper;

    @Test
    public void findPackageListConceptRelation(){
        ConceptRelation conceptRelationRequest = new ConceptRelation();

        List<ConceptRelation> listConceptRelation = proformaMapper.listConceptRelation (conceptRelationRequest);
        Assert.assertEquals(listConceptRelation.size(),1);
    }

    @Test
    public void findPackageListRelatedService (){
        FilterRelatedService filterRelatedServiceRequest = new FilterRelatedService();

        List<RelatedService> listRelatedService = proformaMapper.listRelatedService(filterRelatedServiceRequest);
        Assert.assertEquals(listRelatedService.size(),1);
    }

    @Test
    public void findPackageInsertRelatedService(){
        RelatedService relatedServiceRequest = new RelatedService();

        proformaMapper.insertRelatedService(relatedServiceRequest);

        Assert.assertNotNull(relatedServiceRequest.getId());
    }

    @Test
    public void findPackageUpdateRelatedService(){
        RelatedService relatedServiceRequest = new RelatedService();

        proformaMapper.updateRelatedService(relatedServiceRequest);

        Assert.assertNotNull(relatedServiceRequest.getId());
    }

    @Test
    public void findPackageListMandatory(){
        List<Mandatory> listMandatory = proformaMapper.listMandatory(1);

        Assert.assertEquals(listMandatory.size(),1);
    }

    @Test
    public void findPackageSearchAttributeAssociation(){
        List<AttributeAssociation> searchAttributeAssociation = proformaMapper.searchAttributeAssociation(1);

        Assert.assertEquals(searchAttributeAssociation.size(),1);
    }

    @Test
    public void findPackageSearchCombination(){
        List<Combination> searchCombination = proformaMapper.searchCombination(1);

        Assert.assertEquals(searchCombination.size(),1);
    }
}
