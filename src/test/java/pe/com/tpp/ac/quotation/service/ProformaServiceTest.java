package pe.com.tpp.ac.quotation.service;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import pe.com.tpp.ac.quotation.model.ConceptRelation;
import pe.com.tpp.ac.quotation.model.FilterRelatedService;
import pe.com.tpp.ac.quotation.model.RelatedServiceByConcept;
import pe.com.tpp.ac.quotation.model.view.GroupRelatedService;

import java.util.List;

@SpringBootTest
@ActiveProfiles("test")
public class ProformaServiceTest {

    @Autowired
    private ProformaService proformaService;

    @Test
    public void findPackageListConceptRelation(){
        ConceptRelation conceptRelationRequest = new ConceptRelation();

        List<ConceptRelation> listConceptRelation = proformaService.listConceptRelation(conceptRelationRequest);
        Assert.assertEquals(listConceptRelation.size(),1);
    }

    @Test
    public void findPackageInsertRelatedService(){
        GroupRelatedService relatedServiceRequest = new GroupRelatedService();

        GroupRelatedService insertRelatedService = proformaService.insertRelatedService(relatedServiceRequest);
        Assert.assertNotEquals(insertRelatedService,null);
    }

    @Test
    public void findPackageUpdateRelatedService(){
        GroupRelatedService relatedServiceRequest = new GroupRelatedService();

        GroupRelatedService updateRelatedService = proformaService.insertRelatedService(relatedServiceRequest);
        Assert.assertNotEquals(updateRelatedService,null);
    }

    @Test
    public void findPackageListRelatedServiceGroup(){
        FilterRelatedService filterRelatedServiceRequest = new FilterRelatedService();

        List<RelatedServiceByConcept> listRelatedServiceByConcept = proformaService.listRelatedServiceGroup(filterRelatedServiceRequest);
        Assert.assertEquals(listRelatedServiceByConcept.size(),1);
    }
}
