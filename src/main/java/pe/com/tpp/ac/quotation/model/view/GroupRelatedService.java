package pe.com.tpp.ac.quotation.model.view;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import pe.com.tpp.ac.quotation.model.Group;
import pe.com.tpp.ac.quotation.model.RelatedService;

import java.util.ArrayList;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
@Data
public class GroupRelatedService {
    @JsonProperty("group")
    private Group group;

    @JsonProperty("relatedService")
    private ArrayList<RelatedService> relatedServices;
}
