package pe.com.tpp.ac.quotation.dao;

import org.apache.ibatis.annotations.*;
import pe.com.tpp.ac.quotation.model.*;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface QuotationMapper {

    @Insert("INSERT INTO public.tbl_com_cotizacion(" +
            "cpnid_transaccion_persona, cpnid_transaccion_unidadnegocio, cpnid_cliente, cpc_asunto, " +
            "cpdemision, cpnid_recalada, cpnid_bl, cpnid_transaccion_region, cpnid_localidad, " +
            "cpnid_credito, cpnid_plazocredito, cpnid_sede, cpnid_instrumentocred, cpdfinvigenciaproforma, " +
            "cpdfinvigenciacont, cpnseguro, cpnid_tipocliente, cpnid_mercadovertical, cpnid_producto, " +
            "cpnid_clientefinal, cpnid_monitoreocarga, cpnid_modalidad, cpnid_estadoregistro, " +
            "cpcusuario, cpccodproforma )" +
            "VALUES ( " +
            "#{body.enterprise}, #{body.businessUnit}, #{body.client}, #{body.affair}, " +
            "#{body.dateOfIssue}, #{body.landfall}, #{body.bl}, #{body.region}, #{body.location}, " +
            "#{body.credit}, #{body.creditTerm}, #{body.campus}, #{body.creditInstrument}, #{body.endDateOfProforma}, " +
            "#{body.endDateOfCont}, #{body.safe}, #{body.customerType}, #{body.verticalMarket}, #{body.product}, " +
            "#{body.finalClient}, #{body.loadMonitoring}, #{body.modality}, #{body.registrationStatus}, " +
            "#{body.user}, #{body.proforma});")
    @Options(useGeneratedKeys=true, keyProperty="id")
    int insert(@Param("body")Quotation body);





    @Update("UPDATE public.tbl_com_cotizacion " +
            "SET " +
            "cpnid_transaccion_persona=#{body.enterprise}, " +
            "cpnid_transaccion_unidadnegocio=#{body.businessUnit}, " +
            "cpnid_cliente=#{body.client}, cpc_asunto=#{body.affair}, " +
            "cpdemision=#{body.dateOfIssue}, " +
            "cpnid_recalada=#{body.landfall}, " +
            "cpnid_bl=#{body.bl}, " +
            "cpnid_transaccion_region=#{body.region}, " +
            "cpnid_localidad=#{body.location}, " +
            "cpnid_credito=#{body.credit}, " +
            "cpnid_plazocredito=#{body.creditTerm}, " +
            "cpnid_sede=#{body.campus}, " +
            "cpnid_instrumentocred=#{body.creditInstrument}, " +
            "cpdfinvigenciaproforma=#{body.endDateOfProforma}, " +
            "cpdfinvigenciacont=#{body.endDateOfCont}, " +
            "cpnseguro=#{body.safe}, " +
            "cpnid_tipocliente=#{body.customerType}, " +
            "cpnid_mercadovertical=#{body.verticalMarket}, " +
            "cpnid_producto=#{body.product}, " +
            "cpnid_clientefinal=#{body.finalClient}, " +
            "cpnid_monitoreocarga=#{body.loadMonitoring}, " +
            "cpnid_modalidad=#{body.modality}, " +
            "cpnid_estadoregistro=#{body.registrationStatus}, " +
            "cpcmodsuario=#{body.modifierUser}, " +
            "cpdmodcreacion=#{body.modificationDate}, " +
            "cpccodproforma=#{body.proforma} " +
            "WHERE cpnid_cotizacion = #{body.id}")
    int updateCotizacion(@Param("body") Quotation body);


    @Select("SELECT cot.cpnid_cotizacion as id, " +
            "cpnid_transaccion_persona as enterprise, " +
            "cot.cpnid_transaccion_unidadnegocio as businessUnit, " +
            "cpnid_cliente as client," +
            "cpc_asunto as affair, " +
            "cpdemision as dateOfIssue, " +
            "cpnid_recalada as landfall, " +
            "cpnid_bl as bl, " +
            "cot.cpnid_transaccion_region as region, " +
            "cpnid_localidad as location, " +
            "cpnid_credito as credit, " +
            "cpnid_plazocredito as creditTerm, " +
            "cpnid_sede as campus, " +
            "cpnid_instrumentocred as creditInstrument, " +
            "cpdfinvigenciaproforma as endDateOfProforma, " +
            "cpdfinvigenciacont as endDateOfCont, " +
            "cpnseguro as safe, " +
            "cpnid_tipocliente as customerType, " +
            "cpnid_mercadovertical as verticalMarket, " +
            "cpnid_producto as product, " +
            "cpnid_clientefinal as finalClient, " +
            "cpnid_monitoreocarga as loadMonitoring, " +
            "cpnid_modalidad as modality, " +
            "cot.cpnid_estadoregistro as registrationStatus, " +
            "cot.cpdcreacion as registrationDate, " +
            "cot.cpcusuario as user, " +
            "cot.cpcmodsuario as modifierUser, " +
            "cot.cpdmodcreacion as modificationDate, " +
            "cpccodproforma as proforma, " +
            "pe.cpcpersona as customerDescription, " +
            "rg.cpcregion as regionDescription, " +
            "un.cpcunidadnegocio as businessUnitDescription, " +
            "loc.cpczona as locationDescription, " +
            "COALESCE(det.cpnid_cotizacion,0) as isRelatedService " +
            "FROM public.tbl_com_cotizacion cot " +
            "left join " +
            "(SELECT " +
            "DISTINCT " +
            "cpnid_cotizacion " +
            "FROM public.tbl_mge_cotarticuloservicio b " +
            "WHERE b.cpnid_estadoregistro = 1) as det " +
            "on det.cpnid_cotizacion = cot.cpnid_cotizacion " +
            "left join public.tbl_mge_persona pe on cot.cpnid_cliente = pe.cpnid_persona " +
            "left join public.tbl_com_transaccion_region tg on cot.cpnid_transaccion_region = tg.cpnid_transaccion_region " +
            "inner join public.tbl_mge_region rg on tg.cpnid_region = rg.cpnid_region " +
            "left join public.tbl_com_transaccion_unidadnegocio tun on cot.cpnid_transaccion_unidadnegocio = tun.cpnid_transaccion_unidadnegocio " +
            "left join public.tbl_mge_unidadnegocio un on tun.cpnid_unidadnegocio = un.cpnid_unidadnegocio " +
            "inner join public.tbl_mge_zona loc on cot.cpnid_localidad = loc.cpnid_zona " +
            "where cot.cpnid_estadoregistro  = 1 and cot.cpcusuario=#{body.user} " +
            "${body.condition}")
    ArrayList<Quotation> listQuotation (@Param("body") FilterQuotation body);

    @Select("SELECT cpnid_zona as id, " +
            "cpnid_region as region, " +
            "cpczona as zone, " +
            "cpnid_estadoregistro as registrationStatus " +
            "FROM public.tbl_mge_zona " +
            "WHERE cpnid_estadoregistro= 1")
    ArrayList<Location> listLocation (@Param("filter") Location filter);

    @Select("SELECT cpnid_empresasucursal as id, " +
            "cpnid_empresa as business, " +
            "cpnid_zona as zone, " +
            "cpcempresasucursal as branchCompany, " +
            "cpcdescripcionempresasucursal as branchCompanyDescription, " +
            "cpnid_estadoregistro as registrationStatus " +
            "FROM public.tbl_mge_empresasucursal " +
            "WHERE cpnid_estadoregistro = 1")
    ArrayList<Campus> listCampus (@Param("filter") Campus filter);

    @Select("SELECT cpnid_puerto as id, " +
            "cpnid_pais as country, " +
            "cpcpuerto as portName " +
            "FROM public.tbl_mge_puerto " +
            "${body.condition}")
    List<Port> listPort (@Param("body") Port body);

}

