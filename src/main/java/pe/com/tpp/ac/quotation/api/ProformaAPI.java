package pe.com.tpp.ac.quotation.api;

import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.tpp.ac.quotation.model.*;
import pe.com.tpp.ac.quotation.model.view.GroupRelatedService;
import pe.com.tpp.ac.quotation.service.ProformaService;
import pe.com.tpp.ac.quotation.util.Constants;
import pe.com.tpp.ac.quotation.util.TimeLoggerAgent;
import pe.com.tpp.ac.quotation.util.TimeRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "*", maxAge = 3600, methods= {RequestMethod.GET,RequestMethod.POST})
@RestController
@RequestMapping("/v1/proforma")
@ApiResponse(responseCode = "401",headers = {@Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))}, content = @Content(mediaType = "application/json",schema = @Schema(implementation = ErrorHttp.class)),description = "Authentication Failure")
@ApiResponse(responseCode = "400",headers = {@Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))}, content = @Content(mediaType = "application/json",schema = @Schema(implementation = ErrorHttp.class)),description = "Hay un elemento incorrecto")
@ApiResponse(responseCode = "404",headers = {@Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))}, content = @Content(mediaType = "application/json",schema = @Schema(implementation = ErrorHttp.class)),description = "Hay un elemento incorrecto")
@ApiResponse(responseCode = "500",headers = {@Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))}, content = @Content(mediaType = "application/json",schema = @Schema(implementation = ErrorHttp.class)),description = "Hay un elemento incorrecto")
@SecurityRequirement(name = "TPP")
public class ProformaAPI {

    @Autowired
    ProformaService proformaService;

    @Autowired
    private TimeRecord timeRecord;

    @Autowired
    private TimeLoggerAgent timeLoggerAgent;

    @ApiResponse(responseCode = "200",
            headers = {
                    @Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))},
            content = @Content(mediaType = "application/json",   schema = @Schema(implementation = ConceptRelation.class)))
    @PostMapping(value = "/listConceptRelation",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArrayList<ConceptRelation>> listConceptRelation(@RequestBody ConceptRelation body){
        String uuid = UUID.randomUUID().toString();
        MDC.put(Constants.CORRELATION_ID_MDC, uuid);
        timeRecord.setCorrelationId(uuid);
        timeRecord.setType(Constants.CODE_INFO_LOG);
        timeRecord.setEndPoint("/listConceptRelation");
        timeRecord.setStartTime(System.currentTimeMillis());

        ArrayList<ConceptRelation> listConceptRelation = proformaService.listConceptRelation(body);

        timeRecord.setStatus(Constants.CODE_OK_200);
        timeRecord.setEndTime(System.currentTimeMillis());
        timeLoggerAgent.logService(timeRecord);
        return new ResponseEntity<>(listConceptRelation, HttpStatus.OK);
    }

    @ApiResponse(responseCode = "200",
            headers = {
                    @Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))},
            content = @Content(mediaType = "application/json",   schema = @Schema(implementation = GroupRelatedService.class)))
    @PostMapping(value = "/addRelatedService",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GroupRelatedService> saveRelatedService(@RequestBody GroupRelatedService body){
        String uuid = UUID.randomUUID().toString();
        MDC.put(Constants.CORRELATION_ID_MDC, uuid);
        timeRecord.setCorrelationId(uuid);
        timeRecord.setType(Constants.CODE_INFO_LOG);
        timeRecord.setEndPoint("/addRelatedService");
        timeRecord.setStartTime(System.currentTimeMillis());

        GroupRelatedService relatedServiceByConcept = proformaService.insertRelatedService(body);

        timeRecord.setStatus(Constants.CODE_OK_200);
        timeRecord.setEndTime(System.currentTimeMillis());
        timeLoggerAgent.logService(timeRecord);
        return new ResponseEntity<>(relatedServiceByConcept, HttpStatus.OK);
    }

    @ApiResponse(responseCode = "200",
            headers = {
                    @Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))},
            content = @Content(mediaType = "application/json",   schema = @Schema(implementation = RelatedService.class)))
    @PostMapping(value = "/updateRelatedService",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<RelatedService>> updateRelatedService(@RequestBody List<RelatedService> body){
        String uuid = UUID.randomUUID().toString();
        MDC.put(Constants.CORRELATION_ID_MDC, uuid);
        timeRecord.setCorrelationId(uuid);
        timeRecord.setType(Constants.CODE_INFO_LOG);
        timeRecord.setEndPoint("/updateRelatedService");
        timeRecord.setStartTime(System.currentTimeMillis());

        List<RelatedService> relationService = proformaService.updateRelatedService(body);

        timeRecord.setStatus(Constants.CODE_OK_200);
        timeRecord.setEndTime(System.currentTimeMillis());
        timeLoggerAgent.logService(timeRecord);
        return new ResponseEntity<>(relationService, HttpStatus.OK);
    }

    @ApiResponse(responseCode = "200",
            headers = {
                    @Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))},
            content = @Content(mediaType = "application/json",   schema = @Schema(implementation = ListRelatedService.class)))
    @PostMapping(value = "/relatedServiceGroup",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ListRelatedService> listRelatedServiceGroup (@RequestBody FilterRelatedService body){
        String uuid = UUID.randomUUID().toString();
        MDC.put(Constants.CORRELATION_ID_MDC, uuid);
        timeRecord.setCorrelationId(uuid);
        timeRecord.setType(Constants.CODE_INFO_LOG);
        timeRecord.setEndPoint("/relatedServiceGroup");
        timeRecord.setStartTime(System.currentTimeMillis());

        ListRelatedService list = proformaService.seeListRelatedService(body);
        timeRecord.setStatus(Constants.CODE_OK_200);
        timeRecord.setEndTime(System.currentTimeMillis());
        timeLoggerAgent.logService(timeRecord);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

}
