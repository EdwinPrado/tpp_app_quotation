package pe.com.tpp.ac.quotation.service;

import pe.com.tpp.ac.quotation.model.*;

import java.util.ArrayList;
import java.util.List;

public interface QuotationService {
    ArrayList<Quotation> list (FilterQuotation body);
    Quotation insert (Quotation body);
    Quotation update (Quotation body);

    List<Location> listLocation(Location filter);
    List<Campus> listCampus(Campus filter);

    List<Port> listPort(Port body);
}
