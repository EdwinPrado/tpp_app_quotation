package pe.com.tpp.ac.quotation.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ApiSubError {
    private String object;
    private String field;
    private Object value;
    private String message;

    ApiSubError(String object, String message) {
        this.object = object;
        this.message = message;
    }
}
