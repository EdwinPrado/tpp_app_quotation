package pe.com.tpp.ac.quotation.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Scope(value ="request", proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component
public class TimeRecord {

    private String correlationId;
    private String type;
    private String endPoint;
    private long startTime;
    private long endTime;
    private String status;
    private String servicePath;

    private String typeError;
    private String message;
    private String stackTrace1;
    private String stackTrace2;
}
