package pe.com.tpp.ac.quotation.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tpp.ac.quotation.dao.ProformaMapper;
import pe.com.tpp.ac.quotation.model.*;
import pe.com.tpp.ac.quotation.model.view.GroupRelatedService;
import pe.com.tpp.ac.quotation.service.ProformaService;
import pe.com.tpp.ac.quotation.util.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("ProformaService")
public class ProformaServiceImpl implements ProformaService {

    @Autowired
    ProformaMapper proformaMapper;

    @Override
    public ArrayList<ConceptRelation> listConceptRelation(ConceptRelation body) {
        body.setFilter("");
        return proformaMapper.listConceptRelation(body);
    }

    @Override
    public GroupRelatedService insertRelatedService(GroupRelatedService body) {
        ArrayList<RelatedService> list = new ArrayList<>();
        body.getRelatedServices().stream().forEach(x-> {
            proformaMapper.insertRelatedService(x);
            list.add(x);
        });
        body.setRelatedServices(list);

        if(body.getGroup() !=null){
            proformaMapper.insertGroup(body.getGroup());
            body.getGroup().setGroupDetails(new ArrayList<>());

            body.getGroup().getIdMandatory().stream().forEach(y->{
                for(int i = 0; i < body.getRelatedServices().size(); i++){
                    if(body.getRelatedServices().get(i).getMandatory().compareTo(y) == 0){
                        GroupDetail groupDetail = new GroupDetail();
                        groupDetail.setGroup(body.getGroup().getId());
                        groupDetail.setRelatedService(body.getRelatedServices().get(i).getId());
                        groupDetail.setRegistrationStatus(1);
                        groupDetail.setUser(body.getRelatedServices().get(i).getUser());
                        proformaMapper.insertGroupDetail(groupDetail);
                        body.getGroup().getGroupDetails().add(groupDetail);
                        break;
                    }
                }
            });

        }
/*
        body.getGroups().stream().forEach(x-> {
            proformaMapper.insertGroup(x);
            x.setGroupDetails(new ArrayList<>());
            x.getIdMandatory().stream().forEach(y->{
                for(int i = 0; i < body.getRelatedServices().size(); i++){
                    if(body.getRelatedServices().get(i).getMandatory().compareTo(y) == 0){
                        GroupDetail groupDetail = new GroupDetail();
                        groupDetail.setGroup(x.getId());
                        groupDetail.setRelatedService(body.getRelatedServices().get(i).getId());
                        groupDetail.setRegistrationStatus(1);
                        groupDetail.setUser(body.getRelatedServices().get(i).getUser());
                        proformaMapper.insertGroupDetail(groupDetail);
                        x.getGroupDetails().add(groupDetail);
                        break;
                    }
                }
            });
        }
        )
*/

        return body;
    }

    @Override
    public List<RelatedService> updateRelatedService(List<RelatedService> body) {
        body.stream().forEach(x -> proformaMapper.updateRelatedService(x));
        return body;
    }

    @Override
    public ListRelatedService seeListRelatedService(FilterRelatedService body) {

        ListRelatedService listRelatedService = new ListRelatedService();

        List<RelatedService> relatedServices = proformaMapper.listRelatedService(body);
        List<Integer> idRelatedService = new ArrayList<>();
        List<Integer> idMandatory = new ArrayList<>();

        relatedServices.stream().forEach(x->{
            x.setCombination(proformaMapper.searchCombination(x.getMandatory()));
            x.setDisabledCheck(Boolean.FALSE);
            idRelatedService.add(x.getId());
            idMandatory.add(x.getMandatory());
        });

        List<RelatedService> groupConcept = relatedServices.stream().
                filter(Constants.distinctByKeys(RelatedService::getConceptManagement))
                .collect(Collectors.toList());

        List<RelatedServiceByConcept> listGroup = new ArrayList<>();
        groupConcept.stream().forEach(x -> {
            RelatedServiceByConcept relatedServiceByConcept = new RelatedServiceByConcept();
            ArrayList<AttributeAssociation> attributeAssociation = proformaMapper.searchAttributeAssociation(x.getConceptManagement());
            relatedServiceByConcept.setAttributeAssociation(attributeAssociation);
            relatedServiceByConcept.setConceptManagement(x.getConceptManagement());
            List<RelatedService> relatedServicesRelation =  relatedServices.stream().filter(y -> y.getConceptManagement().compareTo(x.getConceptManagement()) == 0)
                    .collect(Collectors.toList());
            relatedServiceByConcept.setRelatedServices((ArrayList<RelatedService>) relatedServicesRelation);

            listGroup.add(relatedServiceByConcept);
        });


        if(!idRelatedService.isEmpty()){
            StringBuilder stringInId= new StringBuilder("");
            idRelatedService.stream().forEach(x-> stringInId.append("," + x.toString()));
            stringInId.append(")");
            List<GroupDetail> groupDet = proformaMapper.listGroupDetail("And cpnid_cotarticuloservicio in ("+stringInId.substring(1));
            if(!groupDet.isEmpty()){

                Group group = proformaMapper.searchGroup(groupDet.get(0).getGroup());
                group.setGroupDetails((ArrayList<GroupDetail>) groupDet);
                group.setIdMandatory((ArrayList<Integer>) idMandatory);
                listRelatedService.setGroup(group);
            }

        }




        listRelatedService.setRelatedServiceByConcept((ArrayList<RelatedServiceByConcept>) listGroup);

        return listRelatedService;

    }
}
