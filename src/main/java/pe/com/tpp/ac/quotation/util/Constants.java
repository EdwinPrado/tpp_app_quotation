package pe.com.tpp.ac.quotation.util;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Constants {
    private Constants(){

    }

    public static final String CODE_ERROR_500 = "500";
    public static final String CODE_ERROR_404 = "404";
    public static final String CODE_OK_200 = "200";

    public static final String CODE_INFO_LOG = "INFO";
    public static final String CODE_ERROR_LOG = "ERROR";

    public static final String TIME_LOGGER_FORMAT_HOUR = "HH:mm:ss.SSSSSS";

    public static final String CORRELATION_ID_MDC = "correlationId";
    public static final String TYPE_MDC = "type";
    public static final String ENDPOINT_MDC = "endPoint";
    public static final String START_TIME_MDC = "startTime";
    public static final String END_TIME_MDC = "endTime";
    public static final String STATUS_MDC = "status";
    public static final String TYPE_ERROR_MDC = "typeError";
    public static final String SERVICE_PATH_MDC = "servicePath";
    public static final String MESSAGE_MDC = "message";
    public static final String STACK_TRACE1_MDC = "stackTrace1";
    public static final String STACK_TRACE2_MDC = "stackTrace2";


    public static String validateError(String errors){
        String result ="";
        if(errors.contains("violates unique constraint"))
        {
            result="Violates unique constraint";
        }
        return result;
    }

    public static Integer validateResponse(Map<String, Object> response){
        Integer result = 0;
        for (String key : response.keySet()) {
            if(key.equals("errors")){
                result = 1;
            }
            else{
                break;
            }
        }

        return result;
    }

    public static <T> Predicate<T> distinctByKeys(Function<? super T, ?>... keyExtractors)
    {
        final Map<List<?>, Boolean> seen = new ConcurrentHashMap<>();

        return t ->
        {
            final List<?> keys = Arrays.stream(keyExtractors)
                    .map(ke -> ke.apply(t))
                    .collect(Collectors.toList());

            return seen.putIfAbsent(keys, Boolean.TRUE) == null;
        };
    }

}
