package pe.com.tpp.ac.quotation.service;

import pe.com.tpp.ac.quotation.model.*;
import pe.com.tpp.ac.quotation.model.view.GroupRelatedService;

import java.util.ArrayList;
import java.util.List;

public interface ProformaService {
    ArrayList<ConceptRelation> listConceptRelation(ConceptRelation body);
    GroupRelatedService insertRelatedService(GroupRelatedService body);

    List<RelatedService> updateRelatedService (List<RelatedService> body);

    ListRelatedService seeListRelatedService (FilterRelatedService body);

}
