package pe.com.tpp.ac.quotation.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionControllerAdvice {

    @Autowired
    private TimeRecord timeRecord;

    @Autowired
    private TimeLoggerAgent timeLoggerAgent;


    @ExceptionHandler(Exception.class)
    protected ResponseEntity<ErrorResponseAdvice> handleSftpException(Exception e, HandlerMethod handlerMethod, HttpServletRequest request){
        int cont = 0;
        for (StackTraceElement element : e.getStackTrace()) {

            if(cont == 0) timeRecord.setStackTrace1(element.toString());
            if(cont == 1) timeRecord.setStackTrace2(element.toString());
            cont ++;
        }

        timeRecord.setType(Constants.CODE_ERROR_LOG);
        timeRecord.setStatus(Constants.CODE_ERROR_500);
        timeRecord.setTypeError(e.getClass().toString().substring(6));
        timeRecord.setServicePath(request.getRequestURL().toString());
        timeRecord.setMessage(e.getMessage());
        timeRecord.setEndTime(System.currentTimeMillis());

        timeLoggerAgent.logService(timeRecord);

        return new ResponseEntity<>(new ErrorResponseAdvice(Constants.CODE_ERROR_500, request.getRequestURL().toString()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
