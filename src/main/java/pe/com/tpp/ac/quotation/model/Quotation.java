package pe.com.tpp.ac.quotation.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
@Data
public class Quotation {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("enterprise")
    private Integer enterprise;

    @JsonProperty("businessUnit")
    private Integer businessUnit;

    @JsonProperty("client")
    private Integer client;

    @JsonProperty("affair")
    private String affair;

    @JsonProperty("dateOfIssue")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date dateOfIssue;

    @JsonProperty("landfall")
    private Integer landfall;

    @JsonProperty("bl")
    private Integer bl;

    @JsonProperty("region")
    private Integer region;

    @JsonProperty("location")
    private Integer location;

    @JsonProperty("credit")
    private Integer credit;

    @JsonProperty("creditTerm")
    private Integer creditTerm;

    @JsonProperty("campus")
    private Integer campus;

    @JsonProperty("creditInstrument")
    private Integer creditInstrument;

    @JsonProperty("endDateOfProforma")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date endDateOfProforma;

    @JsonProperty("endDateOfCont")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date endDateOfCont;


    @JsonProperty("safe")
    private Boolean safe;

    @JsonProperty("customerType")
    private Integer customerType;

    @JsonProperty("verticalMarket")
    private Integer verticalMarket;

    @JsonProperty("product")
    private Integer product;

    @JsonProperty("finalClient")
    private Integer finalClient;

    @JsonProperty("loadMonitoring")
    private Integer loadMonitoring;

    @JsonProperty("modality")
    private Integer modality;

    @JsonProperty("registrationStatus")
    private Integer registrationStatus;

    @JsonProperty("registrationDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date registrationDate;

    @JsonProperty("user")
    private String user;

    @JsonProperty("modifierUser")
    private String modifierUser;

    @JsonProperty("modificationDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date modificationDate;

    @JsonProperty("proforma")
    private String proforma;

    @JsonProperty("isRelatedService")
    private Integer isRelatedService;

    @JsonProperty("customerDescription")
    private String customerDescription;

    @JsonProperty("regionDescription")
    private String regionDescription;

    @JsonProperty("businessUnitDescription")
    private String businessUnitDescription;

    @JsonProperty("locationDescription")
    private String locationDescription;
}
