package pe.com.tpp.ac.quotation.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class FilterRelatedService {
    @JsonProperty("quotation")
    private Integer quotation;
    @JsonProperty("type")
    private Integer type;
    @JsonProperty("origin")
    private Integer origin;
}
