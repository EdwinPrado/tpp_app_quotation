package pe.com.tpp.ac.quotation.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
@Data
public class Combination {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("attributeAssociation")
    private Integer attributeAssociation;

    @JsonProperty("mandatory")
    private Integer mandatory;

    @JsonProperty("idOption")
    private Integer idOption;

    @JsonProperty("state")
    private Integer state;

    @JsonProperty("registrationDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private String registrationDate;

    @JsonProperty("user")
    private String user;

    //only
    @JsonProperty("pos")
    private Integer pos;

    @JsonProperty("description")
    private String description;

}
