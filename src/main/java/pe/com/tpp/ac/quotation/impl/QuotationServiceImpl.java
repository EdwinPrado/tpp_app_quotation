package pe.com.tpp.ac.quotation.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tpp.ac.quotation.dao.QuotationMapper;
import pe.com.tpp.ac.quotation.model.*;
import pe.com.tpp.ac.quotation.service.QuotationService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("QuotationService")
public class QuotationServiceImpl implements QuotationService {

    @Autowired
    QuotationMapper quotationMapper;

    @Override
    public ArrayList<Quotation> list(FilterQuotation body) {
        StringBuilder value = new StringBuilder("");
        if(body.getNumberDocument() != null && !body.getNumberDocument().isEmpty()){
            value.append("and pe.cpcdocumentoidentidad LIKE '%"+body.getNumberDocument()+"%' ");
        }
        if(body.getNameClient() != null && !body.getNameClient().isEmpty()){
            value.append("and LOWER(pe.cpcpersona) LIKE '%"+body.getNameClient().toLowerCase()+"%' ");
        }
        if(body.getStartDate() != null && body.getEndDate() != null){
            value.append("and cot.cpdemision BETWEEN '%"+body.getStartDate()+"%' and '%"+body.getEndDate()+"%' ");
        }
        body.setCondition(value.toString());
        return quotationMapper.listQuotation(body);
    }

    @Override
    public Quotation insert(Quotation body) {
        quotationMapper.insert(body);
        return body;
    }

    @Override
    public Quotation update(Quotation body) {
        body.setModificationDate(new Date());
        quotationMapper.updateCotizacion(body);
        return body;
    }

    @Override
    public List<Location> listLocation(Location filter) {
        return quotationMapper.listLocation(filter);
    }

    @Override
    public List<Campus> listCampus(Campus filter) {
        return quotationMapper.listCampus(filter);
    }

    @Override
    public List<Port> listPort(Port body) {
        StringBuilder value = new StringBuilder("");
        if(body.getId() != null){
            value.append("WHERE cpnid_puerto = '"+body.getId()+"'");
        }
        if(body.getPortName() !=null && !body.getPortName().isEmpty()){
            value.append("WHERE LOWER(cpcpuerto) LIKE '%" +body.getPortName().toLowerCase()+ "%' LIMIT 20");
        }
        body.setCondition(value.toString());
        return quotationMapper.listPort(body);
    }
}
