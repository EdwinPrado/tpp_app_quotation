package pe.com.tpp.ac.quotation.util;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorResponseAdvice {
    private String status;
    private String servicePath;

}
