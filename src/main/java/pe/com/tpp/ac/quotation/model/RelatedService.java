package pe.com.tpp.ac.quotation.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
@Data

public class RelatedService {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("quotation")
    private Integer quotation;

    @JsonProperty("mandatory")
    private Integer mandatory;

    @JsonProperty("startDateValidity")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date startDateValidity;

    @JsonProperty("endDateValidity")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date endDateValidity;

    @JsonProperty("cost")
    private BigDecimal cost;

    @JsonProperty("bookRate")
    private BigDecimal bookRate;

    @JsonProperty("minimumRent")
    private BigDecimal minimumRent;

    @JsonProperty("minimumInterestRate")
    private BigDecimal minimumInterestRate;

    @JsonProperty("interestCredit")
    private BigDecimal interestCredit;

    @JsonProperty("proformaRate")
    private BigDecimal proformaRate;

    @JsonProperty("tax")
    private BigDecimal tax;

    @JsonProperty("amount")
    private BigDecimal amount;

    @JsonProperty("type")
    private Integer type;

    @JsonProperty("origin")
    private Integer origin;

    @JsonProperty("registrationStatus")
    private Integer registrationStatus;

    @JsonProperty("registrationDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date registrationDate;

    @JsonProperty("user")
    private String user;

    @JsonProperty("modifierUser")
    private String modifierUser;

    @JsonProperty("modificationDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date modificationDate;

    @JsonProperty("costInPercentage")
    private BigDecimal costInPercentage;

    @JsonProperty("bookRatePercentage")
    private BigDecimal bookRatePercentage;

    @JsonProperty("minimumRentPercentage")
    private BigDecimal minimumRentPercentage;

    //only

    @JsonProperty("conbination")
    private ArrayList<Combination> combination;

    @JsonProperty("conceptManagement")
    private Integer conceptManagement;

    @JsonProperty("unitMeasure")
    private Integer unitMeasure;

    @JsonProperty("coin")
    private Integer coin;

    @JsonProperty("typeOfVoucher")
    private Integer typeOfVoucher;

    @JsonProperty("unitMeasureDescription")
    private String unitMeasureDescription;

    @JsonProperty("coinDescription")
    private String coinDescription;

    @JsonProperty("conceptDescription")
    private String conceptDescription;

    @JsonProperty("typeOfVoucherDescription")
    private String typeOfVoucherDescription;

    @JsonProperty("disabledCheck")
    private boolean disabledCheck;

}
