package pe.com.tpp.ac.quotation.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
@Data
public class Agency {

    @JsonProperty("origin")
    private Integer origin;

    @JsonProperty("destination")
    private Integer destination;

    @JsonProperty("grt")
    private String grt;

    @JsonProperty("eta")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date eta;

    @JsonProperty("etd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date etd;

    @JsonProperty("loa")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date loa;
}
