package pe.com.tpp.ac.quotation.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.logging.log4j.util.Strings;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import javax.validation.ConstraintViolation;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Data
@EqualsAndHashCode(callSuper = true)
public class ErrorHttp extends  RuntimeException {
    @JsonProperty("timestamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    String timestamp;
    @JsonProperty("status")
    String status;
    @JsonProperty("error")
    String error;
    @JsonProperty("message")
    String message;
    @JsonProperty("path")
    String path;
    @JsonProperty("suberrors")
    private List<ApiSubError> subErrors;

    private ErrorHttp() {
        timestamp = LocalDateTime.now().toString();
    }

    ErrorHttp(HttpStatus status) {
        this();
        this.status = String.valueOf(status.value());
    }

    public ErrorHttp(HttpStatus status, String message) {
        this();
        this.message = message;
        this.status = String.valueOf(status.value());
    }

    ErrorHttp(HttpStatus status, String message, Throwable ex) {
        this();
        this.status = String.valueOf(status.value());
        this.message = message;
        this.error = ex.getLocalizedMessage();
    }

    ErrorHttp(HttpStatus status, Throwable ex) {
        this();
        this.status = String.valueOf(status.value());
        this.message = ex.getLocalizedMessage();
    }

    private void addValidationError(String object, String field, Object rejectedValue, String message) {
        addSubError(new ApiSubError(object, field, rejectedValue, message));
    }

    private void addValidationError(String object, String message) {
        addSubError(new ApiSubError(object, message));
    }

    private void addSubError(ApiSubError subError) {
        if (subErrors == null) {
            subErrors = new ArrayList<>();
        }
        subErrors.add(subError);
    }

    void addValidationErrors(List<FieldError> fieldErrors) {
        fieldErrors.forEach(this::addValidationError);
    }

    private void addValidationError(FieldError fieldError) {
        this.addValidationError(fieldError.getObjectName(), fieldError.getField(), fieldError.getRejectedValue(),
                String.format(Objects.toString(fieldError.getDefaultMessage(), Strings.EMPTY), fieldError.getField()));
    }

    private void addValidationError(ObjectError objectError) {
        this.addValidationError(objectError.getObjectName(), objectError.getDefaultMessage());
    }

    void addValidationError(List<ObjectError> globalErrors) {
        globalErrors.forEach(this::addValidationError);
    }

    private void addValidationError(ConstraintViolation<?> cv) {
        this.addValidationError(cv.getRootBeanClass().getSimpleName(), cv.getPropertyPath().toString(),
                //((PathImpl) cv.getPropertyPath()).getLeafNode().asString(),
                cv.getInvalidValue(), cv.getMessage());
    }

    void addValidationErrors(Set<ConstraintViolation<?>> constraintViolations) {
        constraintViolations.forEach(this::addValidationError);
    }
}