package pe.com.tpp.ac.quotation.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
@Data
public class Mandatory {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("conceptManagement")
    private Integer conceptManagement;

    @JsonProperty("unitMeasure")
    private Integer unitMeasure;

    @JsonProperty("coin")
    private Integer coin;

    @JsonProperty("typeOfVoucher")
    private Integer typeOfVoucher;

    @JsonProperty("cost")
    private BigDecimal cost;

    @JsonProperty("costInPercentage")
    private BigDecimal costInPercentage;

    @JsonProperty("productIncome")
    private BigDecimal productIncome;

    @JsonProperty("productPercentage")
    private BigDecimal productPercentage;

    @JsonProperty("commercialIncome")
    private BigDecimal commercialIncome;

    @JsonProperty("commercialPercentage")
    private BigDecimal commercialPercentage;

    @JsonProperty("bookRate")
    private BigDecimal bookRate;

    @JsonProperty("bookRatePercentage")
    private BigDecimal bookRatePercentage;

    @JsonProperty("state")
    private Integer state;

    @JsonProperty("registrationDate")
    private String registrationDate;

}
