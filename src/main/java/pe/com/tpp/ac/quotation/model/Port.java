package pe.com.tpp.ac.quotation.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
@Data
public class Port {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("country")
    private  Integer country;
    @JsonProperty("portName")
    private  String portName;
    @JsonProperty("condition")
    private String condition;
}
