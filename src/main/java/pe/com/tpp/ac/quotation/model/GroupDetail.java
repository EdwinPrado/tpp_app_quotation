package pe.com.tpp.ac.quotation.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
@Data
public class GroupDetail {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("relatedService")
    private Integer relatedService;

    @JsonProperty("group")
    private Integer group;

    @JsonProperty("registrationStatus")
    private Integer registrationStatus;

    @JsonProperty("registrationDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date registrationDate;

    @JsonProperty("user")
    private String user;

    @JsonProperty("modifierUser")
    private String modifierUser;

    @JsonProperty("modificationDate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es_PE", timezone = "America/Lima")
    private Date modificationDate;

}
