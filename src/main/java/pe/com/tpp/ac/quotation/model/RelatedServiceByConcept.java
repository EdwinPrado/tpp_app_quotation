package pe.com.tpp.ac.quotation.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
@Data
public class RelatedServiceByConcept {

    @JsonProperty("conceptManagement")
    private Integer conceptManagement;

    @JsonProperty("relatedService")
    private ArrayList<RelatedService> relatedServices;

    @JsonProperty("attributeAssociation")
    private ArrayList<AttributeAssociation> attributeAssociation;

}
