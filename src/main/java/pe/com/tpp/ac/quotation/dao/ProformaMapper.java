package pe.com.tpp.ac.quotation.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.ibatis.annotations.*;
import pe.com.tpp.ac.quotation.model.*;

import java.util.ArrayList;

@Mapper
public interface ProformaMapper {
    @Select("SELECT distinct art.cpnid_articulo as id, " +
            "art.cpnid_concepto as concept, " +
            "art.cpnid_empresa as enterprise, " +
            "art.cpnid_estadoregistro as state, " +
            "cnt.cpcdesconcepto as conceptDescription " +
            "FROM public.tbl_com_articulo art INNER JOIN public.tbl_mge_articuloatributo arta " +
            "ON art.cpnid_articulo = arta.cpnid_articulo INNER JOIN public.tbl_com_concepto cnt " +
            "ON art.cpnid_concepto = cnt.cpnid_concepto " +
            "WHERE art.cpnid_empresa = #{body.enterprise} " +
            "${body.filter}")
    ArrayList<ConceptRelation> listConceptRelation(@Param("body") ConceptRelation body);

    @Select("SELECT cpnid_cotarticuloservicio as id, " +
            "cpnid_cotizacion as quotation, " +
            "cas.cpnid_articuloservicio as mandatory, " +
            "cpncosto as cost, " +
            "cpntarifalibro as bookRate, " +
            "cpnrentamincomercial as minimumRent, " +
            "cpntasainteresmin as minimumInterestRate, " +
            "cpninterescredito as interestCredit, " +
            "cpntarifaproforma as proformaRate, " +
            "cpnimpuesto as tax, " +
            "cpnmontototal as amount, " +
            "cpntipo as type, " +
            "cpnorigen as origin, " +
            "cpdiniciovigencia as startDateValidity, " +
            "cpdfinvigencia as endDateValidity, " +
            "cpncostoporcentaje as costInPercentage, " +
            "cpntarifalibroporcentaje as bookRatePercentage, " +
            "cpnrentamincomercialporcentaje as minimumRentPercentage, " +
            "cas.cpnid_estadoregistro as registrationStatus, " +
            "ars.cpnid_articulo as conceptManagement, " +
            "c.cpcdesconcepto as conceptDescription, " +
            "ars.cpnid_unidadmedida as unitMeasure, " +
            "u.cpcmaestrotabla as unitMeasureDescription, " +
            "ars.cpnid_moneda as coin, " +
            "m.cpcmaestrotabla as coinDescription, " +
            "ars.cpnid_tipocomprobante as typeOfVoucher, " +
            "t.cpcmaestrotabla as typeOfVoucherDescription " +
            "FROM public.tbl_mge_cotarticuloservicio cas " +
            "inner join  public.tbl_mge_articuloservicio ars " +
            "ON ars.cpnid_articuloservicio = cas.cpnid_articuloservicio " +
            "inner join public.tbl_mge_maestrotabla m on(ars.cpnid_moneda = m.cpnid_maestrotabla) " +
            "inner join public.tbl_mge_maestrotabla u on(ars.cpnid_unidadmedida = u.cpnid_maestrotabla) " +
            "inner join public.tbl_mge_maestrotabla t on(ars.cpnid_tipocomprobante = t.cpnid_maestrotabla) " +
            "inner join public.tbl_com_articulo a on (a.cpnid_articulo =ars.cpnid_articulo) " +
            "inner join public.tbl_com_concepto c on(c.cpnid_concepto = a.cpnid_concepto) " +
            "WHERE ars.cpnid_estadoregistro = 1 and cas.cpnid_estadoregistro = 1 " +
            "AND cpnid_cotizacion=#{body.quotation} " +
            "AND cpntipo=#{body.type} AND cpnorigen=#{body.origin}")
    ArrayList<RelatedService> listRelatedService(@Param("body") FilterRelatedService body);

    @Insert("INSERT INTO public.tbl_mge_cotarticuloservicio(" +
            "cpnid_cotizacion, cpnid_articuloservicio, cpncosto, cpntarifalibro, " +
            "cpnrentamincomercial, cpntasainteresmin, cpninterescredito, cpntarifaproforma, " +
            "cpnimpuesto, cpnmontototal, cpntipo, cpnorigen, cpnid_estadoregistro, " +
            "cpcusuario, cpdiniciovigencia, cpdfinvigencia, cpncostoporcentaje, " +
            "cpntarifalibroporcentaje, cpnrentamincomercialporcentaje ) " +
            "VALUES ( " +
            "#{body.quotation}, #{body.mandatory}, #{body.cost}, #{body.bookRate}, " +
            "#{body.minimumRent}, #{body.minimumInterestRate}, #{body.interestCredit}, #{body.proformaRate}, " +
            "#{body.tax}, #{body.amount}, #{body.type}, #{body.origin}, #{body.registrationStatus}, " +
            "#{body.user}, #{body.startDateValidity}, #{body.endDateValidity}, #{body.costInPercentage}, " +
            "#{body.bookRatePercentage}, #{body.minimumRentPercentage});")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int insertRelatedService(@Param("body") RelatedService body);

    @Insert("INSERT INTO public.tbl_mge_agrupa(cpcdescripcion, cpnid_estadoregistro,  cpcusuario) " +
            "VALUES( #{body.description}, #{body.registrationStatus}, #{body.user} );")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int insertGroup(@Param("body") Group body);


    @Insert("INSERT INTO public.tbl_mge_agrupa_det(" +
            " cpnid_cotarticuloservicio, cpnid_agrupa, cpnid_estadoregistro, cpcusuario)" +
            "VALUES ( #{body.relatedService}, #{body.group}, " +
            "#{body.registrationStatus}, #{body.user} );")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int insertGroupDetail(@Param("body") GroupDetail body);

    @Select("SELECT cpnid_agrupa as id, " +
            "cpcdescripcion as description, " +
            "cpnid_estadoregistro as registrationStatus " +
            "FROM public.tbl_mge_agrupa " +
            "where cpnid_estadoregistro = 1 " +
            "${condition} " +
            "order by cpnid_agrupa asc;")
    ArrayList<Group> listGroup(@Param("condition") String condition);


    @Select("SELECT cpnid_agrupa as id, " +
            "cpcdescripcion as description, " +
            "cpnid_estadoregistro as registrationStatus " +
            "FROM public.tbl_mge_agrupa " +
            "where cpnid_estadoregistro = 1 " +
            "and cpnid_agrupa = #{id} " +
            "order by cpnid_agrupa asc;")
    Group searchGroup(@Param("id") int id);


    @Select("SELECT cpnid_agrupa_det as id, " +
            "cpnid_cotarticuloservicio as relatedService, " +
            "cpnid_agrupa as group, " +
            "cpnid_estadoregistro as registrationStatus " +
            "FROM public.tbl_mge_agrupa_det " +
            "where cpnid_estadoregistro = 1 " +
            "${condition} " +
            "order by cpnid_agrupa asc;")
    ArrayList<GroupDetail> listGroupDetail(@Param("condition") String condition);

    @Update("UPDATE public.tbl_mge_cotarticuloservicio " +
            "SET " +
            "cpncosto=#{body.cost}, " +
            "cpntarifalibro=#{body.bookRate}, " +
            "cpnrentamincomercial=#{body.minimumRent}, " +
            "cpntasainteresmin=#{body.minimumInterestRate}, " +
            "cpninterescredito=#{body.interestCredit}, " +
            "cpntarifaproforma=#{body.proformaRate}, " +
            "cpnimpuesto=#{body.tax}, " +
            "cpnmontototal=#{body.amount}, " +
            "cpntipo=#{body.type}, " +
            "cpnorigen=#{body.origin}, " +
            "cpnid_estadoregistro=#{body.registrationStatus}, " +
            "cpdmodcreacion=#{body.modificationDate}, " +
            "cpcmodsuario=#{body.modifierUser}," +
            "cpdiniciovigencia=#{body.startDateValidity}, " +
            "cpdfinvigencia=#{body.endDateValidity}, " +
            "cpncostoporcentaje=#{body.costInPercentage}, " +
            "cpntarifalibroporcentaje=#{body.bookRatePercentage}, " +
            "cpnrentamincomercialporcentaje=#{body.minimumRentPercentage} " +
            "WHERE cpnid_cotarticuloservicio=#{body.id};")
    int updateRelatedService(@Param("body") RelatedService body);

    @Select("SELECT ars.cpnid_articuloservicio as id, " +
            "ars.cpnid_articulo as conceptManagement, " +
            "ars.cpnid_unidadmedida as unitMeasure, " +
            "ars.cpnid_moneda as coin, " +
            "ars.cpnid_tipocomprobante as typeOfVoucher, " +
            "ars.cpnid_estadoregistro as  state" +
            "FROM public.tbl_mge_articuloservicio ars INNER JOIN public.tbl_mge_cotarticuloservicio cas " +
            "ON ars.cpnid_articuloservicio = cas.cpnid_articuloservicio " +
            "WHERE ars.cpnid_estadoregistro = 1 and cas.cpnid_estadoregistro = 1 " +
            "and cas.cpnid_cotizacion=#{quotation}")
    ArrayList<Mandatory> listMandatory(@Param("quotation") Integer quotation);


    @Select("SELECT " +
            "cpnid_articuloatributo as id, " +
            "cpnid_articulo as conceptManagement, " +
            "a.cpnid_maestrotipo as attribute, " +
            "cpcmaestrotipo as description, " +
            "a.cpnid_estadoregistro as state " +
            "FROM public.tbl_mge_articuloatributo a " +
            "inner join  public.tbl_mge_maestrotipo b " +
            "on (a.cpnid_maestrotipo = b.cpnid_maestrotipo) " +
            "where cpnid_articulo = #{id} " +
            "order by cpnid_articuloatributo asc")
    ArrayList<AttributeAssociation> searchAttributeAssociation(@Param("id") Integer id);

    @Select("SELECT cpnid_articulocombinacion as id, " +
            "cpnid_articuloservicio as mandatory, " +
            "cpnid_articuloatributo as attributeAssociation, " +
            "c.cpnid_maestrotabla as idOption, " +
            "cpcmaestrotabla as description, " +
            "c.cpnid_estadoregistro as state " +
            "FROM public.tbl_mge_articulocombinacion c " +
            "inner join public.tbl_mge_maestrotabla td " +
            "on(c.cpnid_maestrotabla = td.cpnid_maestrotabla ) " +
            "where cpnid_articuloservicio = #{id} " +
            "and c.cpnid_estadoregistro = 1 "+
            "order by cpnid_articuloatributo asc ")
    ArrayList<Combination> searchCombination(@Param("id") Integer id);
}