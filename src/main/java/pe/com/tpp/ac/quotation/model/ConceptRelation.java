package pe.com.tpp.ac.quotation.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.NONE, setterVisibility=JsonAutoDetect.Visibility.NONE, creatorVisibility=JsonAutoDetect.Visibility.NONE)
@Data
public class ConceptRelation {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("enterprise")
    private Integer enterprise;

    @JsonProperty("concept")
    private Integer concept;

    @JsonProperty("conceptDescription")
    private String conceptDescription;

    @JsonProperty("state")
    private Integer state;

    private String filter;

}
