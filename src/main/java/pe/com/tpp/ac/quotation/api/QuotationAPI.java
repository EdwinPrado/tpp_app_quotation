package pe.com.tpp.ac.quotation.api;

import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.tpp.ac.quotation.model.*;
import pe.com.tpp.ac.quotation.service.QuotationService;
import pe.com.tpp.ac.quotation.util.Constants;
import pe.com.tpp.ac.quotation.util.TimeLoggerAgent;
import pe.com.tpp.ac.quotation.util.TimeRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "*", maxAge = 3600, methods= {RequestMethod.GET,RequestMethod.POST})
@RestController
@RequestMapping("/v1/quotation")
@ApiResponse(responseCode = "401",headers = {@Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))}, content = @Content(mediaType = "application/json",schema = @Schema(implementation = ErrorHttp.class)),description = "Authentication Failure")
@ApiResponse(responseCode = "400",headers = {@Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))}, content = @Content(mediaType = "application/json",schema = @Schema(implementation = ErrorHttp.class)),description = "Hay un elemento incorrecto")
@ApiResponse(responseCode = "404",headers = {@Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))}, content = @Content(mediaType = "application/json",schema = @Schema(implementation = ErrorHttp.class)),description = "Hay un elemento incorrecto")
@ApiResponse(responseCode = "500",headers = {@Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))}, content = @Content(mediaType = "application/json",schema = @Schema(implementation = ErrorHttp.class)),description = "Hay un elemento incorrecto")
@SecurityRequirement(name = "TPP")
public class QuotationAPI {

    @Autowired
    QuotationService quotationService;

    @Autowired
    private TimeRecord timeRecord;

    @Autowired
    private TimeLoggerAgent timeLoggerAgent;


    @ApiResponse(responseCode = "200",
            headers = {
                    @Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))},
            content = @Content(mediaType = "application/json",   schema = @Schema(implementation = Quotation.class)))
    @PostMapping(value = "/add",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Quotation> saveQuotation(@RequestBody Quotation body){
        String uuid = UUID.randomUUID().toString();
        MDC.put(Constants.CORRELATION_ID_MDC, uuid);
        timeRecord.setCorrelationId(uuid);
        timeRecord.setType(Constants.CODE_INFO_LOG);
        timeRecord.setEndPoint("/add");
        timeRecord.setStartTime(System.currentTimeMillis());

        Quotation quotation = quotationService.insert(body);

        timeRecord.setStatus(Constants.CODE_OK_200);
        timeRecord.setEndTime(System.currentTimeMillis());
        timeLoggerAgent.logService(timeRecord);
        return new ResponseEntity<>(quotation, HttpStatus.OK);
    }

    @ApiResponse(responseCode = "200",
            headers = {
                    @Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))},
            content = @Content(mediaType = "application/json",   schema = @Schema(implementation = Quotation.class)))
    @PostMapping(value = "/update",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Quotation> updateQuotation(@RequestBody Quotation body){
        String uuid = UUID.randomUUID().toString();
        MDC.put(Constants.CORRELATION_ID_MDC, uuid);
        timeRecord.setCorrelationId(uuid);
        timeRecord.setType(Constants.CODE_INFO_LOG);
        timeRecord.setEndPoint("/update");
        timeRecord.setStartTime(System.currentTimeMillis());

        Quotation quotation = quotationService.update(body);

        timeRecord.setStatus(Constants.CODE_OK_200);
        timeRecord.setEndTime(System.currentTimeMillis());
        timeLoggerAgent.logService(timeRecord);
        return new ResponseEntity<>(quotation, HttpStatus.OK);
    }

    @ApiResponse(responseCode = "200",
            headers = {
                    @Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))},
            content = @Content(mediaType = "application/json",   schema = @Schema(implementation = Quotation.class)))
    @PostMapping(value = "/list",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public  ResponseEntity<ArrayList<Quotation>> listQuotation(@RequestBody FilterQuotation body){
        String uuid = UUID.randomUUID().toString();
        MDC.put(Constants.CORRELATION_ID_MDC, uuid);
        timeRecord.setCorrelationId(uuid);
        timeRecord.setType(Constants.CODE_INFO_LOG);
        timeRecord.setEndPoint("/list");
        timeRecord.setStartTime(System.currentTimeMillis());

        ArrayList<Quotation> listQuotation = quotationService.list(body);

        timeRecord.setStatus(Constants.CODE_OK_200);
        timeRecord.setEndTime(System.currentTimeMillis());
        timeLoggerAgent.logService(timeRecord);
        return new ResponseEntity<>(listQuotation, HttpStatus.OK);
    }

    @ApiResponse(responseCode = "200",
            headers = {
                    @Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))},
            content = @Content(mediaType = "application/json",   schema = @Schema(implementation = Location.class)))
    @PostMapping(value = "/listLocationByRegion",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Location>> listLocation(@RequestBody Location filter){
        String uuid = UUID.randomUUID().toString();
        MDC.put(Constants.CORRELATION_ID_MDC, uuid);
        timeRecord.setCorrelationId(uuid);
        timeRecord.setType(Constants.CODE_INFO_LOG);
        timeRecord.setEndPoint("/listLocationByRegion");
        timeRecord.setStartTime(System.currentTimeMillis());

        List<Location> listLocation = quotationService.listLocation(filter);

        timeRecord.setStatus(Constants.CODE_OK_200);
        timeRecord.setEndTime(System.currentTimeMillis());
        timeLoggerAgent.logService(timeRecord);
        return new ResponseEntity<>(listLocation, HttpStatus.OK);
    }

    @ApiResponse(responseCode = "200",
            headers = {
                    @Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))},
            content = @Content(mediaType = "application/json",   schema = @Schema(implementation = Campus.class)))
    @PostMapping(value = "/listCampusByLocation",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Campus>> listCampus(@RequestBody Campus filter){
        String uuid = UUID.randomUUID().toString();
        MDC.put(Constants.CORRELATION_ID_MDC, uuid);
        timeRecord.setCorrelationId(uuid);
        timeRecord.setType(Constants.CODE_INFO_LOG);
        timeRecord.setEndPoint("/listCampusByLocation");
        timeRecord.setStartTime(System.currentTimeMillis());

        List<Campus> listCampus = quotationService.listCampus(filter);

        timeRecord.setStatus(Constants.CODE_OK_200);
        timeRecord.setEndTime(System.currentTimeMillis());
        timeLoggerAgent.logService(timeRecord);
        return new ResponseEntity<>(listCampus, HttpStatus.OK);
    }

    @ApiResponse(responseCode = "200",
            headers = {
                    @Header(name = "Access-Control-Allow-Origin",schema = @Schema(type = "String"))},
            content = @Content(mediaType = "application/json",   schema = @Schema(implementation = Port.class)))
    @PostMapping(value = "/listPort",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Port>> listPort(@RequestBody Port body){
        String uuid = UUID.randomUUID().toString();
        MDC.put(Constants.CORRELATION_ID_MDC, uuid);
        timeRecord.setCorrelationId(uuid);
        timeRecord.setType(Constants.CODE_INFO_LOG);
        timeRecord.setEndPoint("/listPort");
        timeRecord.setStartTime(System.currentTimeMillis());

        List<Port> listPort = quotationService.listPort(body);

        timeRecord.setStatus(Constants.CODE_OK_200);
        timeRecord.setEndTime(System.currentTimeMillis());
        timeLoggerAgent.logService(timeRecord);
        return new ResponseEntity<>(listPort, HttpStatus.OK);
    }
}
